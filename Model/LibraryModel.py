class Library:
    """this class is a model concerning properties of library herself"""
    def __init__(self, name):
        self.name = name
        self.books = []
        self.max_quantity = 2000
