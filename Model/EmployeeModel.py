import json


class Employee:
    """this class is a model concerning datas of each employee of library"""
    def __init__(self, name="", password="", role=""):
        self.name = name
        self.password = password
        self.role = role

    def get_data(self):
        return{
            "name": self.name,
            "password": self.password,
            "role": self.role
        }

    def load_employee(self, i):
        self.name = i["name"]
        self.password = i["password"]
        self.role = i["role"]


class EmployeeManager:
    """this class is a model that manages each employee of library"""
    def __init__(self):
        self.employees = []

    def get_employees(self):
        return self.employees

    def load(self):
        try:
            with open("employees.json", "r") as f:
                data = json.load(f)
            for key in data.keys():
                employee = Employee()
                employee.load_employee(data[key])
                self.employees.append(employee)
        except:
            pass

    def new_employee(self):
        employee = Employee()
        employee.load_employee()
        self.employees.append(employee)
        self.save_employees()

    def save_employees(self):
        data = {}
        i = 1
        for employee in self.employees:
            data[i] = employee.get_data()
            i += 1
        with open("employees.json", "w") as f:
            json.dump(data, f)
