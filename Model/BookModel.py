import copy
import datetime
import json
import random
import sys


class Book():
    def __init__(self, title, category, price, added_on, location):
        self.title = title
        self.category = category
        self.price = price
        self.added_on = added_on
        self.deleted_on = ""
        self.location = location

    def get_data(self):
        return {
            "title": self.title,
            "category": self.category,
            "price": self.price,
            "location": self.location,
            "added_on": self.added_on,
                }

    def load_book(self):
        with open("Data/books.json", "r") as f:
            data = json.load(f)
        data[random.choice(range(sys.maxsize))] = self.get_data()
        with open("Data/books.json", "w") as f:
            json.dump(data, f, indent=2)
        with open("Data/books_history.json", "r") as f:
            data = json.load(f)
        data[len(data)] = self.get_data()
        with open("Data/books_history.json", "w") as f:
            json.dump(data, f, indent=2)


class BookManager():
    def __init__(self):
        self.list_of_books = []
        self.list_of_borrowed_books = []
        self.list_of_books_history = []
        self.list_of_deleted_books = []

    def add_a_book(self):
        self.list_of_books = []
        with open("Data/books.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            book = Book(j["title"], j["category"], j["price"], j["added_on"], j["location"])
            self.list_of_books.append(book)

    def books_history(self):
        self.list_of_books_history = []
        with open("Data/books_history.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            book = Book(j["title"], j["category"], j["price"], j["added_on"], j["location"])
            self.list_of_books_history.append(book)

    def deleted_books_history(self):
        self.list_of_deleted_books = []
        with open("Data/deleted_books_history.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            book = Book(j["title"], j["category"], j["price"], j["added_on"], j["location"])
            book.deleted_on = j["date de vente"]
            self.list_of_deleted_books.append(book)

    def borrowed_books_history(self):
        self.list_of_borrowed_books = []
        with open("Data/borrowed_books.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            book = Book(j["title"], j["category"], j["price"], j["added_on"], j["location"])
            self.list_of_borrowed_books.append(book)

    def delete_a_book(self, title):
        x = 0
        landmark = False
        with open("Data/books.json", "r") as f:
            data = json.load(f)
        data2 = copy.deepcopy(data)
        for i in data:
            if title == data[i]["title"]:
                x = data2.pop(i, None)
               # print("x :", x)
                landmark = True
                with open("Data/books.json", "w") as f:
                    json.dump(data2, f, indent=2)
                with open("Data/deleted_books_history.json", "r") as f:
                    d = json.load(f)
                    x["date de vente"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
                p = range(-sys.maxsize, 0)
                d[random.choice(p)] = x
                with open("Data/deleted_books_history.json", "w") as f:
                    json.dump(d, f, indent=2)

        return landmark

    def borrow_a_book(self, title):
        x = 0
        landmark = False
        with open("Data/books.json", "r") as f:
            data = json.load(f)
        for i in data:
            if title == data[i]["title"]:
                landmark = True
        return landmark


