import copy
import datetime
import json
import random
import sys


class Client():
    def __init__(self, last_name, first_name, address, phone_number, book, borrow_at, return_at):
        self.last_name = last_name
        self.first_name = first_name
        self.address = address
        self.phone_number = phone_number
        self.book = book
        self.borrow_at = borrow_at
        self.return_at = return_at

    def get_data(self):
        return {
            "last_name": self.last_name,
            "first_name": self.first_name,
            "address": self.address,
            "phone": self.phone_number,
            "book": self.book,
            "borrow_at": self.borrow_at,
            "return_at": self.return_at
        }

    def load_client(self):
        with open("Data/clients.json", "r") as f:
            data = json.load(f)
        data[random.choice(range(sys.maxsize))] = self.get_data()
        with open("Data/clients.json", "w") as f:
            json.dump(data, f, indent=2)
        with open("Data/borrowed_books_history.json", "r") as f:
            d = json.load(f)
        d = data
        with open("Data/borrowed_books_history.json", "w") as f:
            json.dump(data, f, indent=2)


class ClientManager():
    def __init__(self):
        self.liste_client = []
        self.liste_client_historique = []

    def add_a_client(self):
        self.liste_client = []
        with open("Data/clients.json", "r") as f:
            data = json.load(f)
        for i, j in data.items():
            client = Client(j["last_name"], j["first_name"], j["address"], j["phone"], j["book"], j["borrow_at"], j["return_at"])
            self.liste_client.append(client)

    def clients_history(self):
        self.liste_client_historique=[]
        with open("Data/borrowed_books_history.json", "r") as f:
            d = json.load(f)
        for i, j in d.items():
            client = Client(j["last_name"], j["first_name"], j["address"], j["phone"], j["book"], j["borrow_at"], j["return_at"])
            self.liste_client_historique.append(client)

    def borrowed_books_by_clients_history(self, last_name):
        x = 0
        is_exist = False
        with open("Data/books.json", "r") as f:
            data = json.load(f)
        data2 = copy.deepcopy(data)

        for i in data:
            if last_name == data[i]["last_name"]:
                x = data2.pop(i, None)
                with open("Data/books.json", "w") as f:
                    json.dump(data2, f, indent=2)

                with open("Data/borrowed_books.json", "r") as f:
                    datas = json.load(f)
                p = range(-sys.maxsize, 0)
                datas[random.choice(p)] = x
                with open("Data/borrowed_books.json", "w") as f:
                    json.dump(datas, f, indent=2)
                return x


