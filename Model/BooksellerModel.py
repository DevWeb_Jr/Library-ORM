import copy
import json
import random
import sys


class Bookseller():
    def __init__(self, username, pasword):
        self.username = username
        self.passsword = pasword

    def get_data(self):
        return {"username": self.username,
                "password": self.passsword}

    def add_bookseller(self, username):
        try:
            with open("Data/booksellers.json", "r") as f:
                data = json.load(f)
            for i , j in data.items():
                if j["username"] == username:
                    return False
            data[random.choice(range(sys.maxsize))] = self.get_data()
            with open("Data/booksellers.json", "w") as f:
                json.dump(data, f, indent=2)
        except:
            print("Impossible d'ajouter ce libraire")
        return True

    def add_admin(self, username):
        try:
            with open("Data/users.json", "r") as f:
                data = json.load(f)
            for i, j in data.items():
                if j["username"] == username:
                    return False
            data[random.choice(range(sys.maxsize))] = self.get_data()
            with open("Data/users.json", "w") as f:
                json.dump(data, f, indent=2)
        except:
            print("Impossible d'ajouter cet admin")
        return True


class BooksellerManager():
    def __init__(self):
        self.list_of_booksellers = []
        self.list_of_admins = []

    def load_booksellers(self):
        self.list_of_booksellers = []
        try:
            with open("Data/booksellers.json", "r") as f:
                data = json.load(f)
            for i, j in data.items():
                bookseller = Bookseller(j["username"], j["password"])
                self.list_of_booksellers.append(bookseller)
        except:
            print("Impossible d'afficher ce libraire")

    def load_admins(self):
        self.list_of_admins = []
        try:
            with open("Data/users.json", "r") as f:
                data = json.load(f)
            for i, j in data.items():
                admin = Bookseller(j["username"], j["password"])
                self.list_of_admins.append(admin)
        except:
            print("Impossible d'afficher cet admin")

    def delete_bookseller(self, username, password_admin):
        landmark = False
        try:
            with open("Data/booksellers.json", "r") as f:
                data = json.load(f)
            data1 = copy.deepcopy(data)
            with open("Data/users.json", "r") as file:
                data2 = json.load(file)
            for m, n in data2.items():
                for i, j in data.items():
                    if username == j["username"] and password_admin == n["password"]:
                        data1.pop(i, None)
                        landmark = True

                        with open("Data/booksellers.json", "w") as f:
                            json.dump(data1, f, indent=2)
        except:
            print("Impossible de supprimer ce libraire")
        return landmark

    def delete_admin(self, username, password_admin):
        landmark = False
        try:
            with open("Data/users.json", "r") as f:
                data = json.load(f)
            data1 = copy.deepcopy(data)
            with open("Data/users.json", "r") as file:
                data2 = json.load(file)
            for m, n in data2.items():
                for i, j in data.items():
                    if username == j["username"] and password_admin == n["password"]:
                        data1.pop(i, None)
                        landmark = True
                        pass
                        with open("Data/users.json", "w") as f:
                            json.dump(data1, f, indent=2)
        except:
            print("Impossible de supprimer cet admin")
        return landmark
