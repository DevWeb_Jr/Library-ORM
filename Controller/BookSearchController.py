from View.BookSearchView import BookSearchView


class BookSearchController:
    """this class manages the screen of  book's search"""
    def __init__(self, screen):
        self.view = BookSearchView(self)
        self.screen = screen
