from View.MainMenuView import MainMenuView


class MainMenuController:
    """this class manages the screen concerning main menu"""
    def __init__(self, screen):
        self.view = MainMenuView(self)
        self.screen = screen

    def redirect_to_book_details_view(self):
        """this function allows you to redirect to the book's management screen"""
        self.screen.show_header(self.screen.header.view.display_screen_title(text=self.screen.book_details.view.name))

        self.screen.header.display_connection_view()

        self.screen.header.display_home_button_view()

        self.screen.show_body(self.screen.book_details.view)

    def redirect_to_warning_books_view(self):
        """this function allows you to redirect to the late books screen"""
        self.screen.show_header(self.screen.header.view.display_screen_title(text=self.screen.warning_book.view.name))

        self.screen.header.display_connection_view()

        self.screen.header.display_home_button_view()

        self.screen.show_body(self.screen.warning_book.view)

    def redirect_to_sales_results_view(self):
        """this function allows you to redirect to the sales results screen"""
        self.screen.show_header(self.screen.header.view.display_screen_title(text=self.screen.sales_results.view.name))

        self.screen.header.display_connection_view()

        self.screen.header.display_home_button_view()

        self.screen.show_body(self.screen.sales_results.view)

    def redirect_to_employees_management_view(self):
        """this function allows you to redirect to the employee's management screen"""
        self.screen.show_header(self.screen.header.view.display_screen_title(text=self.screen.employee_management.view.name))

        self.screen.header.display_connection_view()

        self.screen.header.display_home_button_view()

        self.screen.show_body(self.screen.employee_management.view.screen_principale)

    def display_footer_view(self):
        """this function allows you to display footer"""
        self.screen.show_footer()
