from View.OrderBookView import OrderBookView


class OrderBookController:
    """this class manages the screen concerning order's book from library"""
    def __init__(self, screen):
        self.view = OrderBookView(self)
        self.screen = screen
