from View.CreateUpdateEmployeeView import CreateUpdateEmployeeView


class CreateUpdateEmployeeController:
    """this class manages the screen concerning addition an modification of employees"""
    def __init__(self, screen):
        self.view = CreateUpdateEmployeeView(self)
        self.screen = screen
