import copy
import datetime
import json
import random
import sys
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from Model.ClientModel import ClientManager, Client
from UserInterface.StandardButton import BasicButton
from Model.BookModel import BookManager, Book
from View.BookDetailsView import BookDetailsView


class BookDetailsController:
    """this class manages the screen of all book's details"""
    def __init__(self, screen):
        self.view = BookDetailsView(self)
        self.screen = screen
        self.livres = BookManager()
        self.livres.add_a_book()
        self.livres.books_history()
        self.livres.borrowed_books_history()
        self.charger_book_liste()
        self.charger_liste_emprunt()

    def charger_book_liste(self):
        for i in range(1, len(self.livres.list_of_books)):
            btn = BasicButton(id=self.livres.list_of_books[i], text=self.livres.list_of_books[i].category_name, font_size=20)
            btn.bind(on_press=self.update_detail)
            self.view.list_of_books.add_widget(btn)

    def update_detail(self, instance):
        self.view.title.text = "category_name : " + instance.id.category_name
        self.view.category.text = "categorie : " + instance.id.categorie
        self.view.buy_price.text = "book_price_input de vente : " + instance.id.book_price_input + " €"
        self.view.book_place.text = "emplacement : " + instance.id.emplacement

    def add_book_to(self):
        self.un_livre = Book(self.view.category_name.text, self.view.choose_a_category_button.text, self.view.book_price_input.text,
                             datetime.datetime.now().strftime("%Y-%m-%d %H:%M"), self.view.main_button.text)
        self.un_livre.load_book()
        self.livres.add_a_book()
        self.view.list_of_books.clear_widgets()
        self.charger_book_liste()

    def acheter_livre(self, *args):
        if self.livres.delete_a_book(self.view.name_book.text) == False:
            self.view.lab_book.text = "Ce livre n'existe pas "
        else:
            self.livres.add_a_book()
            self.view.list_of_books.clear_widgets()
            self.charger_book_liste()
            self.view.pop_acheter.dismiss()

    # chargement la liste des livre emprunté
    def charger_liste_emprunt(self):
        self.clients = ClientManager()
        self.clients.add_a_client()
        self.livres.borrowed_books_history()
        for i in range(1, len(self.livres.list_of_borrowed_books)):
            btn = BasicButton(id=self.clients.liste_client[i], text=self.livres.list_of_borrowed_books[i].category_name,
                              font_size=20)
            btn.bind(on_press=self.pop_liste_client_book)
            self.view.liste_book_emprunter.add_widget(btn)

    def pop_liste_client_book(self, instance):
        self.box_liste_client_book = BoxLayout(orientation="vertical", size_hint=(1, 1), padding=20, spacing=15)
        self.box_btn_emprunt = BoxLayout(size_hint=(1, .2), pos_hint={"center_x": .5}, spacing=10)

        self.lab_client_detail = Label(text="Détail Client", font_size=22, size_hint=(.2, .1), color="green",
                                       pos_hint={"center_x": .5}, bold=True)
        self.box_liste_client_book.add_widget(self.lab_client_detail)
        self.name_client = Label(text="category_name : " + instance.id.category_name, font_size=17, size_hint=(.2, .1),
                                 pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.name_client)

        self.client_prenom = Label(text="prenom : " + instance.id.prenom, font_size=17, size_hint=(.2, .1),
                                   pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.client_prenom)
        self.client_adress = Label(text="adress : " + instance.id.adress, font_size=17, size_hint=(.2, .1),
                                   pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.client_adress)

        self.client_phone = Label(text="phone : " + instance.id.phone_number, font_size=19, size_hint=(.2, .1),
                                  pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.client_phone)

        self.date_emprunt = Label(text="date d'emprunt : " + instance.id.borrow_at, font_size=19,
                                  size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.date_emprunt)

        self.date_retour = Label(text="date de retour : " + instance.id.return_at, font_size=19,
                                 size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.date_retour)

        self.lab_book_detail = Label(text="Détail Livre", font_size=22, size_hint=(.2, .1), color="green",
                                     pos_hint={"center_x": .5}, bold=True)
        self.box_liste_client_book.add_widget(self.lab_book_detail)

        self.book_client = Label(text="category_name de livre : " + instance.id.book["category_name"], font_size=19, size_hint=(.2, .1),
                                 pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.book_client)

        self.categorie_book_client = Label(text="categorie : " + instance.id.book["categorie"], font_size=19,
                                           size_hint=(.2, .1),
                                           pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.categorie_book_client)

        self.prix_client_book = Label(text="book_price_input : " + instance.id.book["book_price_input"], font_size=19,
                                      size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.prix_client_book)

        self.emplacement_book_client = Label(text="Emplacement : " + instance.id.book["Emplacement"], font_size=19,
                                             size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_client_book.add_widget(self.emplacement_book_client)
        self.box_liste_client_book.add_widget(self.box_btn_emprunt)
        self.btn_rendu = BasicButton(id=instance.id, text="Rendu", font_size=18, background_color="green",
                                     size_hint=(.22, .3), pos_hint={"center_x": .5})
        self.btn_rendu.bind(on_press=self.delete_emprunt_book)
        self.box_liste_client_book.add_widget(self.btn_rendu)
        self.pop_client = Popup(title="Detail livre  emprunt", content=self.box_liste_client_book, title_align="center",
                                size_hint=(.8, .8))
        self.pop_client.open()

    def delete_emprunt_book(self, instance):
        x = ""
        with open("Data/clients.json", "r") as f:
            data = json.load(f)
        data2 = copy.deepcopy(data)
        for i, j in data.items():
            if instance.id.category_name == data[i]["category_name"] and instance.id.book["category_name"] == data[i]["book"]["category_name"]:
                data2.pop(i, None)
        with open("Data/clients.json", "w") as f:
            json.dump(data2, f, indent=2)
        with open("Data/borrowed_books.json", "r") as f:
            data = json.load(f)
        data1 = copy.deepcopy(data)

        for i, j in data.items():
            if instance.id.book["category_name"] == data[i]["category_name"] and instance.id.book["categorie"] == data[i]["categorie"]:
                x = data1.pop(i, None)
        with open("Data/borrowed_books.json", "w") as f:
            json.dump(data1, f, indent=2)
        with open("Data/books.json", "r") as f:
            donnee = json.load(f)
        donnee[random.choice(range(sys.maxsize))] = x
        with open("Data/books.json", "w") as f:
            json.dump(donnee, f, indent=2)
        self.livres.add_a_book()
        self.view.list_of_books.clear_widgets()
        self.charger_book_liste()
        self.view.liste_book_emprunter.clear_widgets()
        self.charger_liste_emprunt()
        self.pop_client.dismiss()
        ##############

    def emprunter_livre1(self, *args):
        if self.livres.borrow_a_book(self.view.name_book_emprunter.text) == False:
            self.view.lab_book_emprunter.text = "ce livre n'existe pas"
        else:
            self.view.pop_client_emprunt()

    def save_user(self, *args):
        if len(self.view.nom_client.text) < 3 or len(self.view.prenom_client.text) < 3 or len(
                self.view.adress_client.text) < 3 or \
                len(self.view.phone_client.text) < 10:
            self.view.lab_client.text = "Vous avez oublié des remplir des champs ou vos données sont erronés"
        else:
            self.add_client()
            self.livres.add_a_book()
            self.view.list_of_books.clear_widgets()
            self.charger_book_liste()

            self.livres.borrowed_books_history()
            self.view.liste_book_emprunter.clear_widgets()

            self.charger_liste_emprunt()
            self.view.name_book_emprunter.text = ""
            self.view.lab_book_emprunter.text = ""
            self.view.nom_client.text = ""
            self.view.prenom_client.text = ""
            self.view.adress_client.text = ""
            self.view.phone_client.text = ""
            self.view.pop_client.dismiss()

    def add_client(self):
        self.un_client = Client(self.view.nom_client.text, self.view.prenom_client.text, self.view.adress_client.text,
                                self.view.phone_client.text,
                                self.clients.borrowed_books_by_clients_history(self.view.name_book_emprunter.text),
                                str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")), self.view.date_zone.text)
        self.un_client.load_client()

    # fonctions pour les comportement de bouton history
    def charger_add_book_historique(self):
        self.view.liste_historique.clear_widgets()
        self.livres.books_history()
        for i in range(1, len(self.livres.list_of_books_history)):
            btn = BasicButton(id=self.livres.list_of_books_history[i],
                              text="Livre : " + self.livres.list_of_books_history[i].category_name + " ajouté le : " +
                                   self.livres.list_of_books_history[i].date_ajout,
                              font_size=17, background_color="green")
            btn.bind(on_press=self.pop_detail_historique_book_ajout)
            self.view.liste_historique.add_widget(btn)

    def charger_delete_book_historique(self):
        self.view.liste_historique.clear_widgets()
        self.livres.deleted_books_history()
        for i in range(1, len(self.livres.list_of_deleted_books)):
            btn = BasicButton(id=self.livres.list_of_deleted_books[i],
                              text="Livre : " + self.livres.list_of_deleted_books[i].category_name + " vendu le : " +
                                   self.livres.list_of_deleted_books[i].date_supression,
                              font_size=17, background_color="red")
            btn.bind(on_press=self.pop_detail_historique_book_delete)
            self.view.liste_historique.add_widget(btn)

    def charger_emprunt_book_historique(self):
        self.view.liste_historique.clear_widgets()
        self.clients.clients_history()
        for i in range(1, len(self.clients.liste_client_historique)):
            btn = BasicButton(id=self.clients.liste_client_historique[i],
                              text="Livre : " + self.clients.liste_client_historique[i].book["category_name"] +
                                    " emprunté par :" + self.clients.liste_client_historique[i].category_name + " le : " +
                                   self.clients.liste_client_historique[i].borrow_at,
                              font_size=17, background_color="blue")

            btn.bind(on_press=self.pop_emprunt_historique)
            self.view.liste_historique.add_widget(btn)

    # modele  pour les fonctions des actions des bouton effacer selon chaque history
    def model_delete_historique(self, instance):
        self.box_liste_book = BoxLayout(orientation="vertical", size_hint=(1, 1), padding=20, spacing=15)
        self.lab_book_detail = Label(text="Détail Livre", font_size=25, size_hint=(.2, .1), color="green",
                                     pos_hint={"center_x": .5}, bold=True)
        self.box_liste_book.add_widget(self.lab_book_detail)

        self.book_name = Label(text="category_name de livre : " + instance.id.category_name, font_size=19, size_hint=(.2, .1),
                               pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.book_name)

        self.categorie_book = Label(text="categorie : " + instance.id.categorie, font_size=19,
                                    size_hint=(.2, .1),
                                    pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.categorie_book)

        self.prix_book = Label(text="book_price_input : " + instance.id.book_price_input, font_size=19,
                               size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.prix_book)

        self.emplacement_book = Label(text="Emplacement : " + instance.id.emplacement, font_size=19,
                                      size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.emplacement_book)
        self.date_ajout = Label(text="Date d'ajout : " + instance.id.date_ajout, font_size=19,
                                size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.date_ajout)
        self.date_vendre = Label(text="", font_size=19, size_hint=(.2, .1), pos_hint={"center_x": .5})
        self.box_liste_book.add_widget(self.date_vendre)

        self.box_btn = BoxLayout(size_hint=(.8, .2), pos_hint={"center_x": .5}, spacing=10)
        self.effacer_ajout = BasicButton(id=instance.id, text="Effacer", font_size=17, background_color="red")
        self.box_btn.add_widget(self.effacer_ajout)

        self.btn_cancel = Button(text="Annuler", font_size=17)
        self.box_btn.add_widget(self.btn_cancel)

        self.box_liste_book.add_widget(self.box_btn)
        self.pop_client = Popup(title="Detail livre  emprunt", content=self.box_liste_book, title_align="center",
                                size_hint=(.5, .5))
        self.btn_cancel.bind(on_press=self.pop_client.dismiss)
        self.pop_client.open()

    def pop_detail_historique_book_ajout(self, instance):
        self.model_delete_historique(instance)
        self.effacer_ajout.bind(on_press=self.delete_historique_ajout)

    # fonction pour le bouton effacer l'history de vendre
    def pop_detail_historique_book_delete(self, instance):
        self.model_delete_historique(instance)
        self.effacer_ajout.bind(on_press=self.delete_historique_vendre)
        self.date_vendre.text = "Date de vendre : " + instance.id.date_supression

    def pop_emprunt_historique(self, instance):
        self.pop_liste_client_book(instance)
        self.box_liste_client_book.remove_widget(self.btn_rendu)
        self.effacer_emprunt = BasicButton(id=instance.id, text="Effacer", font_size=17, background_color="red")
        self.btn_cancel_emprunt = Button(text="Annuler", font_size=17)
        self.btn_cancel_emprunt.bind(on_press=self.pop_client.dismiss)
        self.effacer_emprunt.bind(on_press=self.delete_historique_emprunt)
        self.box_btn_emprunt.add_widget(self.effacer_emprunt)
        self.box_btn_emprunt.add_widget(self.btn_cancel_emprunt)

    def delete_historique_ajout(self, instance):
        with open("Data/books_history.json", "r") as f:
            data = json.load(f)
        data2 = copy.deepcopy(data)
        for i, j in data.items():
            if instance.id.category_name == data[i]["category_name"] and instance.id.book_price_input == data[i]["book_price_input"]:
                data2.pop(i, None)
        with open("Data/books_history.json", "w") as f:
            json.dump(data2, f, indent=2)
        self.charger_add_book_historique()
        self.pop_client.dismiss()

    def delete_historique_vendre(self, instance):
        with open("Data/deleted_books_history.json", "r") as f:
            data = json.load(f)
        data2 = copy.deepcopy(data)
        for i, j in data.items():
            if instance.id.category_name == data[i]["category_name"] and instance.id.book_price_input == data[i]["book_price_input"]:
                data2.pop(i, None)
        with open("Data/deleted_books_history.json", "w") as f:
            json.dump(data2, f, indent=2)

        self.charger_delete_book_historique()
        self.pop_client.dismiss()

    def delete_historique_emprunt(self, instance):
        with open("Data/emprunt_historique.json", "r") as f:
            data = json.load(f)
        data2 = copy.deepcopy(data)
        for i, j in data.items():
            if instance.id.category_name == data[i]["category_name"] and instance.id.book["category_name"] == data[i]["book"]["category_name"]:
                data2.pop(i, None)
        with open("Data/emprunt_historique.json", "w") as f:
            json.dump(data2, f, indent=2)
        self.charger_emprunt_book_historique()
        self.pop_client.dismiss()

    def search_for_a_book(self, instance, *args):
        self.view.list_of_books.clear_widgets()
        is_here = False
        for i in range(1, len(self.livres.list_of_books)):
            if instance.text in self.livres.list_of_books[i].category_name:
                # self.view.list_of_books.clear_widgets()
                btn = BasicButton(id=self.livres.list_of_books[i], text=self.livres.list_of_books[i].category_name, font_size=20)
                btn.bind(on_press=self.update_detail)
                self.view.list_of_books.add_widget(btn)
                is_here = True
            elif instance.text not in self.livres.list_of_books[i].category_name and is_here == False:
                self.view.list_of_books.clear_widgets()
            elif instance.text == "":
                self.view.list_of_books.clear_widgets()
                self.charger_book_liste()

