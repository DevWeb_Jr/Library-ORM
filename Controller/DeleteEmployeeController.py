from View.DeleteEmployeeView import DeleteEmployeeView


class DeleteEmployeeController:
    """This class manages the screen concerning deletion of employees"""
    def __init__(self, screen):
        self.view = DeleteEmployeeView(self)
        self.screen = screen
