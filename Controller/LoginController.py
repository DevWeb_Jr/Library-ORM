from View.LoginView import LoginView
import json


class LoginController:
    """this class manages the screen concerning user's login on the app"""
    def __init__(self, screen):
        self.view = LoginView(self)
        self.screen = screen
        self.actual_user = []

    def log_in(self, widget):
        """this function allows you to connect on your own main menu"""
        self.actual_user.clear()
        with open("Data/users.json", "r") as f:
            data = json.load(f)
        for i in data.values():
            if i["username"] == self.view.user_input.text and i["password"] == self.view.user_password_input.text:
                self.screen.app_window.body_grid.clear_widgets()
                self.screen.main_menu.view.employees_management_button.disabled = False
                self.screen.main_menu.view.employees_management_button.text = "Gestion des employés"
                self.screen.main_menu.view.employees_management_button.background_color = "#3D6998"
                self.screen.show_body(self.screen.main_menu.view)
                self.actual_user.append(i["username"])
                self.actual_user.append(i["password"])
                self.screen.header.view.user_name.text=self.view.user_input.text
                self.screen.footer.view.display_return_button()
                self.screen.show_footer(self.screen.footer.view)
            else:
                with open("Data/booksellers.json", "r") as f:
                    data1 = json.load(f)
                    for i in data1.values():
                        if i["username"] == self.view.user_input.text and i[
                            "password"] == self.view.user_password_input.text:
                            self.screen.app_window.body_grid.clear_widgets()
                            self.screen.main_menu.view.employees_management_button.disabled=True
                            self.screen.main_menu.view.employees_management_button.text=""
                            self.screen.main_menu.view.employees_management_button.background_color=(0,0,0,0)
                            self.screen.show_body(self.screen.main_menu.view)
                            self.actual_user.append(i["username"])
                            self.actual_user.append(i["password"])
                            self.screen.header.view.user_name.text = self.view.user_input.text
                            self.screen.footer.view.display_return_button()
                            self.screen.show_footer(self.screen.footer.view)
                        else:
                            self.view.info_user.text = "category_name ou mot de passe incorrect, essayez à nouveau svp"

