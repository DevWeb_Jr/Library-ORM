from View.FooterView import FooterView


class FooterController:
    """this class manages the screen concerning footer on every screen"""
    def __init__(self, screen):
        self.view = FooterView(self)
        self.screen = screen

    def redirect_to_previous_screen_view(self):
        """this function redirects to the previous screen from current screen"""
        if self.view.displayed:
            if self.screen.book_details or self.screen.book_search or self.screen.borrow_buy_book or self.screen.order_book:
                self.screen.header.view.display_screen_title(self.screen.main_menu.view.name)
                self.screen.header.view.display_connection()
                self.screen.app_window.display_body(self.screen.main_menu.view)
            elif self.screen.warning_book or self.screen.employee_management or self.screen.sales_results:
                self.screen.header.view.display_screen_title(self.screen.main_menu.view.name)
                self.screen.header.view.display_connection()
                self.screen.app_window.display_body(self.screen.order_book.view)

        """
        if self.screen.book_search or self.screen.book_details or self.screen.borrow_buy_book or self.screen.warning_book or self.screen.order_book or self.screen.employee_management  or self.screen.sales_results:
            self.screen.show(self.screen.main_menu)
        elif self.screen.create_update_employee:
            self.screen.show(self.screen.employee_management)
        """
