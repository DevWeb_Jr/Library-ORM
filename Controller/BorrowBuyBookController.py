from View.BorrowBuyBookView import BorrowBuyBookView


class BorrowBuyBookController:
    """this class manages the screen of all borrowed and book's details"""
    def __init__(self, screen):
        self.view = BorrowBuyBookView(self)
        self.screen = screen

    def add_a_book_view(self):
        """this function allows you to add a book on library"""
        pass

    def delete_a_book_view(self):
        """this function allows you to delete a book from library"""
        pass

    def validate_view(self):
        """this function confirm the user's choice on library"""
        pass
