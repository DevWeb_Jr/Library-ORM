from View.HeaderView import HeaderView


class HeaderController:
    """this class manages the screen concerning header on every screen"""
    def __init__(self, screen):
        self.view = HeaderView(self)
        self.screen = screen

    def display_home_button_view(self):
        """this function implements to a button the come back to home screen"""
        self.view.display_home_button()

    def display_screen_title_view(self):
        """this function implements the display of each screen's title"""
        self.view.display_screen_title()

    def display_connection_view(self):
        """this function implements the display of connection's categories_box on header"""
        self.view.display_connection()

    def redirect_to_home_view(self):
        """this function allows you to return to the home screen"""
        self.screen.show_body(self.screen.main_menu.view)
        self.view.home_button_grid.clear_widgets()

    def unconnect_view(self):
        """this function allows you to disconnect from app"""
        self.screen.footer.view.clear_widgets()
        self.screen.show_body(self.screen.log_in.view)
        self.screen.show_header(self.view.display_screen_title())
        self.screen.log_in.view.user_input.text=""
        self.screen.log_in.view.user_password_input.text = ""
        self.screen.log_in.view.info_user.text = ""
