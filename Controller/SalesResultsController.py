from View.SalesResultsView import SalesResultsView


class SalesResultsController:
    """this class manages the screen concerning sale's graphic"""
    def __init__(self, screen):
        self.view = SalesResultsView(self)
        self.screen = screen
