from View.WarningBookView import WarningBookView


class WarningBookController:
    """this class manages the screen concerning late books"""
    def __init__(self, screen):
        self.view = WarningBookView(self)
        self.screen = screen
