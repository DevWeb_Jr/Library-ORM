from kivy.uix.label import Label

from Model.BooksellerModel import BooksellerManager, Bookseller
from UserInterface.StandardButton import BasicButton
from View.EmployeeManagementView import EmployeeManagementView


class EmployeeManagementController():
    """this class manages the screen concerning management of employees"""
    def __init__(self,screen):
        self.screen=screen
        self.view=EmployeeManagementView(self)
        self.salariees=BooksellerManager()

    def charger_screen_salariee(self,*args):
        self.view.box_btn_menu.clear_widgets()
        self.view.display_add_an_employee_button()
        self.view.box_cordonnee.clear_widgets()
        self.view.liste_salariee.clear_widgets()
        self.view.box_champs.clear_widgets()
        self.charger_screen(self.view.box_salariee)

    def charger_screen_admin(self,*args):
        self.charger_screen(self.view.boxAdmin)

    def charger_screen(self,vue,*args):
        self.screen.show_body(vue)

    def charge_ajout_salariee(self,*args):
        self.view.box_champs.clear_widgets()
        self.view.box_cordonnee.clear_widgets()
        self.view.creat_champs_add_emplyee_manager()
        self.view.box_champs.add_widget(self.view.box_cordonnee)

    def charge_delete_salariee(self,*args):
        self.view.box_champs.clear_widgets()
        self.view.box_cordonnee.clear_widgets()
        self.view.creat_champs_delete_salariee()
        self.view.box_champs.add_widget(self.view.box_cordonnee)


    def save_salariee(self,*args):
        if len(self.view.username_input.text)<4 or len(self.view.password.text)<4:
            self.view.info.text="vous devez remplir les deux champs et dois être plus que 4 catactere"
        else:
            salariee=Bookseller(self.view.username_input.text, self.view.password.text)
            if salariee.add_bookseller(self.view.username_input.text)==False:
                self.view.info.text = "Ce category_name d'utilisateur existe deja , choisissez un autre"
            else:
                self.view.info.text="Done"
                self.view.username_input.text= ""
                self.view.password.text= ""

    def delete_salariee(self,*args):
        if len(self.view.username_input_for_delete.text)<4 or len(self.view.password_of_admin.text)<4:
            self.view.info_label.text="vous devez remplir les deux champs et dois être plus que 4 catactere"
        else:
            if self.salariees.delete_bookseller(self.view.username_input_for_delete.text, self.view.password_of_admin.text)==False:
                self.view.info_label.text = "Ce salariee n'existe pas ou votre password incorrect"
            else:
                self.view.info_label.text = "Done"
                self.view.username_input_for_delete.text=""
                self.view.password_of_admin.text=""

    def charge_liste_salariee(self,*args):
        self.view.box_body.clear_widgets()
        self.view.box_champs.clear_widgets()
        self.view.liste_salariee.clear_widgets()
        self.view.box_champs.add_widget(self.view.scroll)
        self.salariees.load_booksellers()
        if len(self.salariees.list_of_booksellers)==1:
            self.view.box_champs.add_widget(Label(text="Vous avez pas de salarié",font_size=18))
        else:
            for i in range(1, len(self.salariees.list_of_booksellers)):
                btn=BasicButton(text=self.salariees.list_of_booksellers[i].username, font_size=18)
                self.view.liste_salariee.add_widget(btn)



    def ajout_admin(self,*args):
        self.view.box_body.clear_widgets()
        #self.view.box_cordonnee.clear_widgets()
        self.view.box_cordonneeAdmin.clear_widgets()
        self.view.add_an_admin()
        self.view.box_body.add_widget(self.view.box_cordonneeAdmin)

    def delete_admin(self,*args):
        self.view.box_body.clear_widgets()
        self.view.box_cordonneeAdmin.clear_widgets()
        self.view.delete_an_admin()
        self.view.box_body.add_widget(self.view.box_cordonneeAdmin)

    def save_admin(self,*args):
        if len(self.view.username_admin.text)<4 or len(self.view.password_admin.text)<4:
            self.view.info_add_admin.text="vous devez remplir les deux champs et dois être plus que 4 catactere"
        else:
            salariee = Bookseller(self.view.username_admin.text, self.view.password_admin.text)
            if salariee.add_admin(self.view.username_admin.text)==False:
                self.view.info_add_admin.text = "Ce category_name d'utilisateur existe deja , choisissez un autre"
            else:
                self.view.info_add_admin.text="Done"
                self.view.username_admin.text=""
                self.view.password_admin.text=""

    def delete_admin_from_list(self,*args):
        if len(self.view.username_admin_delete.text)<4 or len(self.view.password_admin_delete.text)<4:
            self.view.info_delete_admin.text="vous devez remplir les deux champs et dois être plus que 4 catactere"
        else:
            if self.salariees.delete_admin(self.view.username_admin_delete.text, self.view.password_admin_delete.text)==False:
                self.view.info_delete_admin.text = "Ce salariee n'existe pas ou votre password incorrect"
            else:
                self.view.info_delete_admin.text = "Done"
                self.view.username_admin_delete.text=""
                self.view.password_admin_delete.text=""

    def charge_admin_list(self,*args):
        self.view.box_champs.clear_widgets()
        self.view.box_body.clear_widgets()
        self.view.liste_salariee.clear_widgets()
        self.view.box_body.add_widget(self.view.scroll)
        self.salariees.load_admins()
        if len(self.salariees.list_of_admins) == 1:
            self.view.liste_salariee.add_widget(Label(text="Vous avez pas de salarié", font_size=18))
        else:
            for i in range(1, len(self.salariees.list_of_admins)):
                btn = BasicButton(text=self.salariees.list_of_admins[i].username, font_size=18)
                self.view.liste_salariee.add_widget(btn)

