from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from UserInterface.StandardButton import ClassicButton


class MainMenuView(GridLayout):
    """this class concerns display of the main menu"""
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, **kwargs)
        self.cols = 1
        self.rows = 1
        self.padding = 20
        self.controller = controller
        self.name = "Librairie | Menu principal"

        self.box = BoxLayout()
        self.box.orientation = "vertical"
        self.box.size_hint = (1, .5)
        self.box.padding = 50
        self.box.spacing = 50
        self.add_widget(self.box)

        self.books_management_button = ClassicButton(text="Gestion des livres")
        self.books_management_button.size_hint = (1, .5)
        self.books_management_button.bind(on_press=self.redirect_to_book_details)
        self.box.add_widget(self.books_management_button)

        self.warning_books_button = ClassicButton(text="Alertes livres non rendus")
        self.warning_books_button.size_hint = (1, .5)
        self.warning_books_button.bind(on_press=self.redirect_to_warning_books)
        self.box.add_widget(self.warning_books_button)

        self.sales_results_button = ClassicButton(text="Bilan des ventes")
        self.sales_results_button.size_hint = (1, .5)
        self.sales_results_button.bind(on_press=self.redirect_to_sales_results)
        self.box.add_widget(self.sales_results_button)

        self.employees_management_button = ClassicButton(text="Gestion des employés")
        self.employees_management_button.size_hint = (1, .5)
        self.employees_management_button.bind(on_press=self.redirect_to_employees_management)
        self.box.add_widget(self.employees_management_button)
        
    def redirect_to_book_details(self, widget):
        """this callback function returns to the controller for display the details about a book"""
        self.controller.redirect_to_book_details_view()

    def redirect_to_warning_books(self, widget):
        """this callback function returns to the controller for display late books"""
        self.controller.redirect_to_warning_books_view()

    def redirect_to_sales_results(self, widget):
        """this callback function returns to the controller for display the sales results of library"""
        self.controller.redirect_to_sales_results_view()

    def redirect_to_employees_management(self, widget):
        """this callback function returns to the controller for display the employees management screens"""
        self.controller.redirect_to_employees_management_view()
