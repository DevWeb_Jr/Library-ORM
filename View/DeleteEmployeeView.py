from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput


class DeleteEmployeeView(BoxLayout):
    """this class concerns display about deletion of an employee"""
    def __init__(self, controller, **kwargs):
        super(DeleteEmployeeView, self).__init__(**kwargs,
                                                 orientation="vertical",
                                                 padding=50,
                                                 spacing=30)
        self.controller = controller
        self.name = "Librairie | Supprimer un compte"

        self.display_widgets()

    def display_widgets(self):
        """this function adds widgets concerning deletion of an employee on the grid provided for this purpose"""
        employee_name_input = TextInput(hint_text="Nom du salarié",
                                        multiline=False,
                                        halign="center",
                                        padding=20,
                                        font_size="30",
                                        write_tab=False)
        admin_password_input = TextInput(hint_text="Mot de passe de l'administrateur",
                                         multiline=False,
                                         password=True,
                                         halign="center",
                                         allow_copy=False,
                                         padding=20,
                                         font_size=30,
                                         write_tab=False)
        delete_an_employee_button = Button(text="Valider")
        self.add_widget(employee_name_input)
        self.add_widget(admin_password_input)
        self.add_widget(delete_an_employee_button)
