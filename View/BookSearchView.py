from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput


class BookSearchView(GridLayout):
    """this class concerns display of book's search"""
    def __init__(self, controller, **kwargs):
        super(BookSearchView, self).__init__(**kwargs, cols=2, rows=1)
        self.controller = controller
        self.name = "Librairie | Recherche de livre"

        self.layout1 = BoxLayout(orientation="vertical",  padding=10, spacing=20)
        self.add_widget(self.layout1)

        self.layout2 = BoxLayout(orientation="vertical")
        self.add_widget(self.layout2)

        self.book_search_input = TextInput(hint_text="Rechercher un Livre", multiline=False, halign="center", padding=25, font_size="20", write_tab=False)

        self.validate_button = Button(text="Valider", font_size=16)

        self.update_book_button = Button(text="Modifier", font_size=17)

        self.order_book_button = Button(text="Commander", font_size=18)

        self.display_widgets()

    def display_widgets(self):
        """this function adds a layout with a search bar on the grid provided for this purpose"""
        self.layout1.add_widget(self.book_search_input)
        self.layout1.add_widget(self.validate_button)
        manager = True
        admin = True
        if manager or admin:
            self.layout1.add_widget(self.update_book_button)
        if admin:
            self.layout1.add_widget(self.order_book_button)
        for i in range(5):
            n = i + 1
            button = Button(text=f"Livre {n}")
            self.layout2.add_widget(button)
