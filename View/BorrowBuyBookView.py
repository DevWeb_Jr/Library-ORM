from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from UserInterface.StandardButton import ClassicButton


class BorrowBuyBookView(GridLayout):
    """this class concerns display about management of book purchases and loans"""
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, **kwargs)
        self.cols = 3
        self.rows = 1
        self.controller = controller
        self.name = "Librairie | Emprunt - Achat de livre"

        self.left_grid = GridLayout(cols=1,
                                    rows=5)
        self.left_grid.size_hint = (.2, 1)
        self.spacing = 20
        self.add_widget(self.left_grid)

        self.middle_grid = BoxLayout(orientation="vertical")
        self.middle_grid.size_hint = (.6, 1)
        self.add_widget(self.middle_grid)

        self.right_grid = GridLayout(cols=1,
                                     rows=4)
        self.right_grid.size_hint = (.2, 1)
        self.add_widget(self.right_grid)

        self.display_added_books_box()
        self.display_listed_books_box()
        self.display_client_box()

    def display_added_books_box(self):
        """this function adds a categories_box with a search bar on the grid provided for this purpose"""
        search_bar = TextInput(text="entrer un category_name de livre")
        search_bar.multiline = False
        search_bar.allow_copy = False
        search_bar.write_tab = False
        search_bar.font_size = 17
        self.left_grid.add_widget(search_bar)

        added_books_box = BoxLayout(orientation="vertical")
        self.left_grid.add_widget(added_books_box)

        add_a_book_button = ClassicButton(text="Ajouter")
        add_a_book_button.bind(on_press=self.add_a_book)
        self.left_grid.add_widget(add_a_book_button)

        delete_a_book_button = Button(text="Supprimer")
        delete_a_book_button.bind(on_press=self.delete_a_book)
        self.left_grid.add_widget(delete_a_book_button)

        validate_button = Button(text="Terminer")
        validate_button.bind(on_press=self.validate)
        self.left_grid.add_widget(validate_button)

    def display_listed_books_box(self):
        """this function displays the list of library's books on the grid provided for this purpose"""
        book = Label(text="La force du vent")
        book.font_size = 22
        self.middle_grid.add_widget(book)

    def display_client_box(self):
        """this function displays the list of library's clients and their informations on the grid provided for this purpose"""
        client_informations = Label(text="Nom du client")
        self.right_grid.add_widget(client_informations)

        client_last_name_input = TextInput(text="Nom du client")
        client_last_name_input.multiline = False
        client_last_name_input.allow_copy = False
        client_last_name_input.write_tab = False
        client_last_name_input.font_size = 17
        self.right_grid.add_widget(client_last_name_input)

        client_first_name_input = TextInput(text="Prénom du client")
        client_first_name_input.multiline = False
        client_first_name_input.allow_copy = False
        client_first_name_input.write_tab = False
        client_first_name_input.font_size = 17
        self.right_grid.add_widget(client_first_name_input)

        client_phone_input = TextInput(text="Téléphone du client")
        client_phone_input.multiline = False
        client_phone_input.allow_copy = False
        client_phone_input.write_tab = False
        client_phone_input.font_size = 17
        self.right_grid.add_widget(client_phone_input)

    def add_a_book(self, widget):
        """this callback function returns to the controller for allow the addition of a book"""
        self.controller.add_a_book_view()

    def delete_a_book(self, widget):
        """this callback function returns to the controller for allow the deletion of a book"""
        self.controller.delete_a_book_view()

    def validate(self, widget):
        """this callback function returns to the controller for allow the user's choices"""
        self.controller.validate_view()
