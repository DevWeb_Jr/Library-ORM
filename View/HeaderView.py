from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from UserInterface.StandardButton import ClassicButton
from UserInterface.StandardLabel import TitleLabel


class HeaderView(GridLayout):
    """this class concerns display of header on every screen"""
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, **kwargs)
        self.controller = controller
        self.name = "Librairie | Créer ou modifier un compte"
        self.cols = 3
        self.rows = 1
        self.size_hint = (1, .15)
        self.pos_hint = {"top": 1}
        self.screen_title = None
        self.user_name = Label(text="3 profils", bold=True, color="#3D6998", font_size=17, size_hint=(.1, .1))

        self.home_button_grid = GridLayout(padding=15, spacing=5, cols=1, rows=1, size_hint=(.15, .1))
        self.add_widget(self.home_button_grid)

        self.screen_title_grid = GridLayout(padding=15, spacing=5, cols=1, rows=1, size_hint=(.7, .1))
        self.add_widget(self.screen_title_grid)

        self.connection_grid = GridLayout(padding=15, spacing=5, cols=1, rows=2, size_hint=(.15, .1))
        self.add_widget(self.connection_grid)

    def display_home_button(self):
        """this function displays the button that allows you to return to the home screen"""
        home_button = Button(text="",
                             background_normal="./Image/home_button.png")
        home_button.bind(on_press=self.redirect_to_home)
        self.home_button_grid.add_widget(home_button)
        return self

    def display_screen_title(self, text="Librairie | Connexion"):
        """this function displays the title of each screen"""
        self.home_button_grid.clear_widgets()
        self.screen_title_grid.clear_widgets()
        self.connection_grid.clear_widgets()
        self.screen_title = TitleLabel(text=text)
        self.screen_title_grid.add_widget(self.screen_title)
        return self

    def display_connection(self):
        """this function displays the information about the user role"""
        unconnect_button = ClassicButton(text="se déconnecter",
                                         font_size=17,
                                         size_hint=(.1, .1))
        unconnect_button.bind(on_press=self.unconnect)
        self.connection_grid.add_widget(self.user_name)
        self.connection_grid.add_widget(unconnect_button)
        return self

    def redirect_to_home(self, widget):
        """this callback function returns to the controller and allows you to return to the home screen"""
        self.controller.redirect_to_home_view()
        self.screen_title.text = "Librairie | Menu Principal"

    def unconnect(self, widget):
        """this class allows you to disconnect from app"""
        self.controller.unconnect_view()
