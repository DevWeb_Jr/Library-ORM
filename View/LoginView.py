from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from UserInterface.StandardButton import ClassicButton


class LoginView(GridLayout):
    """this class concerns display of user's log in"""
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, **kwargs,
                            padding=60,
                            spacing=30,
                            cols=1,
                            rows=4)

        self.controller = controller
        self.name = "Librairie | Connexion"
        self.user_input = TextInput(hint_text="Nom",
                                    multiline=False,
                                    size_hint=(.5, .5),
                                    halign="center",
                                    padding=5,
                                    font_size=20,
                                    write_tab=False)
        self.user_password_input = TextInput(hint_text="Mot de passe",
                                             multiline=False,
                                             size_hint=(.5, .5),
                                             password=True,
                                             halign="center",
                                             allow_copy=False,
                                             padding=5,
                                             font_size=20,
                                             write_tab=False)
        self.info_user = Label()
        self.login_button = ClassicButton(text="Se Connecter",
                                          font_size=30)
        self.login_button.bind(on_press=self.controller.log_in)
        self.display_widgets()

    def display_widgets(self):
        self.add_widget(self.user_input)
        self.add_widget(self.user_password_input)
        self.add_widget(self.info_user)
        self.add_widget(self.login_button)

    def login(self, widget):
        self.controller.log_in_view()
