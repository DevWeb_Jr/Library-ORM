from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView


class WarningBookView(ScrollView):
    """this class concerns display of screen concerning late books"""
    def __init__(self, controller, **kwargs):
        super().__init__(**kwargs)
        self.controller = controller
        self.name = "Librairie | Alerte de non retour d'un livre"
        self.listed_warning_box=GridLayout(size_hint_y=None,cols=1,row_default_height=60)
        self.listed_warning_box.bind(minimum_height=self.listed_warning_box.setter("height"))
        self.add_widget(self.listed_warning_box)
        #we should add the button (or the label) of person that depassed the delay at the "listed_warning_box"
