from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput


class OrderBookView(BoxLayout):
    """this class concerns display of screen concerning orders book"""
    def __init__(self, controller, **kwargs):
        super(OrderBookView, self).__init__(**kwargs, orientation="vertical")
        self.name = "Librairie | Commande de livre"
        self.controller = controller

        self.display_view()

    def display_view(self):
        """this function adds layouts about ordering book on the grid provided for this purpose"""
        layout1 = BoxLayout(orientation="vertical")
        self.add_widget(layout1)

        layout2 = AnchorLayout(size_hint=(1, .2))
        self.add_widget(layout2)

        layout3 = AnchorLayout(size_hint=(1, .2), anchor_x="right")

        self.add_widget(layout3)

        order_book_input1 = TextInput(hint_text="Input", size_hint=(1, .1))
        layout1.add_widget(order_book_input1)

        add_a_book_button = Button(text="+", size_hint=(.2, .7))
        layout2.add_widget(add_a_book_button)

        validate_order_button = Button(text="Valider", size_hint=(.3, .7))
        layout3.add_widget(validate_order_button)
