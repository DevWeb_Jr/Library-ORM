import datetime
import string
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.dropdown import DropDown
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.scrollview import ScrollView
from kivy.uix.textinput import TextInput
from kivymd.uix.picker import MDDatePicker

from UserInterface.StandardButton import BasicButton


class BookDetailsView(GridLayout):
    """this class concerns display of all book's details"""
    def __init__(self, controller, **kwargs):
        super().__init__(**kwargs)
        self.controller = controller
        self.name = "Librairie | Fiche détaillée d'un livre"
        self.cols = 2
        self.rows = 1

        self.list_of_borrowed_books = GridLayout(size_hint_y=None, cols=1, row_default_height=50)
        self.list_of_borrowed_books.bind(minimum_height=self.list_of_borrowed_books.setter("height"))

        self.principal_grid = BoxLayout(orientation="vertical", spacing=10, padding=5)

        self.search_grid = GridLayout(cols=1, rows=2)
        self.search_box = GridLayout(cols=1, rows=2, size_hint=(1, 1))
        self.search_bar = TextInput(hint_text="chercher un livre", font_size=18, size_hint=(1, .16))
        self.search_box.add_widget(self.search_bar)
        self.search_bar.bind(text=self.controller.search_for_a_book)

        self.book_manager_grid = BoxLayout(size_hint=(1, .2), padding=5, spacing=10)
        self.title = Label(text="Titre", font_size=20)
        self.principal_grid.add_widget(self.title)
        self.category = Label(text="Catégorie", font_size=20)
        self.principal_grid.add_widget(self.category)
        self.book_place = Label(text="Emplacement", font_size=20)
        self.principal_grid.add_widget(self.book_place)
        self.selling_price = Label(text="Prix de vente", font_size=20)
        self.principal_grid.add_widget(self.selling_price)

        self.history = BasicButton(text="Historique", font_size=20, size_hint=(.5, .8), pos_hint={"center_x": .5})
        self.history.bind(on_press=self.history_pop)
        self.principal_grid.add_widget(self.history)

        self.add_widget(self.principal_grid)
        self.list_of_books = GridLayout(size_hint_y=None, cols=1, row_default_height=50)
        self.list_of_books.bind(minimum_height=self.list_of_books.setter("height"))
        self.grid_details_scroll = ScrollView()
        self.grid_details_scroll.add_widget(self.list_of_books)

        self.search_box.add_widget(self.grid_details_scroll)
        self.borrow_a_book_button = BasicButton(text="Emprunter", font_size=20, size_hint=(1, 1))
        self.borrow_a_book_button.bind(on_press=self.emprunter_livre)
        self.book_manager_grid.add_widget(self.borrow_a_book_button)
        self.sell_a_book_button = BasicButton(text="Vendre", font_size=20, size_hint=(1, 1))
        self.sell_a_book_button.bind(on_press=self.acheter_livre)
        self.add_a_book_button = BasicButton(text="Ajout", font_size=20, size_hint=(1, 1))
        self.add_a_book_button.bind(on_press=self.display_categories_pop_up)
        self.book_manager_grid.add_widget(self.sell_a_book_button)
        self.book_manager_grid.add_widget(self.add_a_book_button)

        self.search_grid.add_widget(self.search_box)
        self.search_grid.add_widget(self.book_manager_grid)
        self.add_widget(self.search_grid)

    def display_categories_pop_up(self, *args):
        """this function displays a popup that shows you all category of books in library"""
        self.categories_box = BoxLayout(orientation="vertical", size_hint=(.8, .9), padding=20, spacing=15)
        self.category_name = TextInput(hint_text="Catégorie", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5}, write_tab=False, multiline=False)
        self.categories_box.add_widget(self.category_name)
        ######################################## dropdown categorie liste
        self.category_dropdown = DropDown()
        categories = ["Politique", "Horreur", "Sport", "Manga", "Fiction", "Enfants", "Histoire"]
        for i in categories:
            btn = BasicButton(text=i, size_hint_y=None, bold=True, font_size=17)
            btn.bind(on_release=self.get_pressbutton_categorie)
            self.category_dropdown.add_widget(btn)
        self.choose_a_category_button = BasicButton(text="choisir la catégorie", size_hint=(.8, .2), pos_hint={"center_x": .5}, font_size=17)
        self.choose_a_category_button.bind(on_release=self.category_dropdown.open)
        self.category_dropdown.bind(on_select=lambda instance, x: setattr(self.choose_a_category_button, "text", x))
        self.categories_box.add_widget(self.choose_a_category_button)

        self.book_price_input = TextInput(hint_text="book_price_input de vente", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5}, write_tab=False, multiline=False, input_filter="int")
        self.categories_box.add_widget(self.book_price_input)
        self.dropdown_emplacement = DropDown()
        alphabet = list(string.ascii_uppercase)
        for j in alphabet[:10]:
            for i in range(1, 11):
                btn = BasicButton(text="%s-%d" % (j, i), size_hint_y=None, bold=True, font_size=17)
                btn.bind(on_release=self.get_pressedbutton_emplacement)
                self.dropdown_emplacement.add_widget(btn)
        self.main_button = BasicButton(text="choisir l'emplacement", size_hint=(.8, .2), pos_hint={"center_x": .5}, font_size=17)
        self.main_button.bind(on_release=self.dropdown_emplacement.open)
        self.dropdown_emplacement.bind(on_select=lambda instance, x: setattr(self.main_button, "text", x))
        self.categories_box.add_widget(self.main_button)

        self.lab = Label(text="", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5})
        self.categories_box.add_widget(self.lab)
        self.btn = BasicButton(text="OK", font_size=19, size_hint=(.15, .15), color="green", pos_hint={"center_x": .5}, bold=True)
        self.categories_box.add_widget(self.btn)
        self.pop = Popup(title="Ajout de livre", content=self.categories_box, title_align="center", size_hint=(.7, .7))
        self.btn.bind(on_press=self.verify_input_book)
        self.pop.open()

    def verify_input_book(self, *args):
        """this function displays a popup that allows you to choice catgory and location of a book in library"""
        if self.category_name.text != "" \
                and self.choose_a_category_button.text != "choisir la catégorie" \
                and self.book_price_input.text != "" \
                and self.main_button.text != "choisir l'emplacement":
            self.controller.add_book_to()
            ################################################
            # ajouter la fonnction concernant l'history #
            ################################################
            self.pop.dismiss()
        else:
            self.lab.text = "il faut remplir tous les champs"

    def get_pressedbutton_emplacement(self, instance):
        """this function gets the button pressed in dropdown"""
        self.dropdown_emplacement.select(instance.text)
        return instance.text

    def get_pressbutton_categorie(self, instance):
        """this function gets the text on instance"""
        self.category_dropdown.select(instance.text)
        return instance.text

    def acheter_livre(self, *args):
        """this function allows a client to buy a book"""
        self.box_acheter = BoxLayout(orientation="vertical", size_hint=(1, .8), padding=20, spacing=15)
        self.name_book = TextInput(hint_text="category_name de livre à acheter", font_size=17, size_hint=(.8, .1), pos_hint={"center_x": .5}, write_tab=False, multiline=False)
        self.box_acheter.add_widget(self.name_book)
        self.lab_book = Label(text="", font_size=17, size_hint=(.8, .15), pos_hint={"center_x": .5})
        self.box_acheter.add_widget(self.lab_book)
        self.btn_acheter = BasicButton(text="ok", font_size=19, size_hint=(.2, .1), color="green", pos_hint={"center_x": .5}, bold=True)
        self.box_acheter.add_widget(self.btn_acheter)
        self.pop_acheter = Popup(title="Acheter un livre", content=self.box_acheter, title_align="center", size_hint=(.5, .5))
        self.btn_acheter.bind(on_press=self.controller.acheter_livre)
        self.pop_acheter.open()

    def emprunter_livre(self, *args):
        """this function allows a client to borrow a book"""
        try:
            self.pop_emprunter.open()
        except AttributeError:
            self.scroll_emprunter = ScrollView()
            self.scroll_emprunter.clear_widgets()
            self.scroll_emprunter.add_widget(self.list_of_borrowed_books)
            self.grid_emprunter = GridLayout(cols=2, rows=1)
            self.box_emprunter = BoxLayout(orientation="vertical", size_hint=(1, .8), padding=20, spacing=15)
            self.name_book_emprunter = TextInput(hint_text="category_name de livre à acheter", font_size=17, size_hint=(.8, .1), pos_hint={"center_x": .5}, multiline=False, write_tab=False)
            self.box_emprunter.add_widget(self.name_book_emprunter)
            self.lab_book_emprunter = Label(text="", font_size=17, size_hint=(.8, .15), pos_hint={"center_x": .5})
            self.box_emprunter.add_widget(self.lab_book_emprunter)
            self.btn_emprunter = BasicButton(text="Emprunt", font_size=19, size_hint=(.25, .1), color="green", pos_hint={"center_x": .5}, bold=True)
            self.btn_quit = BasicButton(text="Quitter", font_size=19, size_hint=(.25, .1), color="red", pos_hint={"center_x": .5}, bold=True)
            self.box_emprunter.add_widget(self.btn_emprunter)
            self.box_emprunter.add_widget(self.btn_quit)
            self.grid_emprunter.add_widget(self.box_emprunter)
            self.grid_emprunter.add_widget(self.scroll_emprunter)

            self.pop_emprunter = Popup(title="Ajout de livre", content=self.grid_emprunter, title_align="center", size_hint=(.7, .7))

            self.btn_emprunter.bind(on_press=self.controller.emprunter_livre1)
            self.btn_quit.bind(on_press=self.pop_emprunter.dismiss)

            self.pop_emprunter.open()

    def pop_client_emprunt(self):
        """this popup function displays client information"""
        self.box_client = BoxLayout(orientation="vertical", size_hint=(1, 1), padding=20, spacing=15)
        self.nom_client = TextInput(hint_text="category_name de client", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5}, write_tab=False, multiline=False)
        self.box_client.add_widget(self.nom_client)
        self.prenom_client = TextInput(hint_text="prenom", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5}, write_tab=False, multiline=False)
        self.box_client.add_widget(self.prenom_client)
        self.adress_client = TextInput(hint_text="adress", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5}, write_tab=False,  multiline=False)
        self.box_client.add_widget(self.adress_client)
        self.phone_client = TextInput(hint_text="numero de téléphone", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5}, write_tab=False, input_filter="int",  multiline=False)
        self.box_client.add_widget(self.phone_client)
        ############################
        # ajout de calendrier
        self.grid_calendrier = GridLayout(cols=2, rows=1, size_hint=(.8, .2), pos_hint={"center_x": .5})
        self.date_zone = TextInput(hint_text="date de retour de livre",
                                   disabled=True, font_size=18)
        self.grid_calendrier.add_widget(self.date_zone)
        calender = BasicButton(text="", background_normal="Image/calender.png", size_hint=(.3, 1))
        calender.bind(on_press=self.show_date_picker)
        self.grid_calendrier.add_widget(calender)
        self.box_client.add_widget(self.grid_calendrier)

        self.lab_client = Label(text="", font_size=17, size_hint=(.8, .2), pos_hint={"center_x": .5})
        self.box_client.add_widget(self.lab_client)
        self.btn_client = BasicButton(text="OK", font_size=19, size_hint=(.15, .15), color="green", pos_hint={"center_x": .5}, bold=True)
        self.box_client.add_widget(self.btn_client)
        self.pop_client = Popup(title="Client", content=self.box_client, title_align="center", size_hint=(1, 1))
        self.btn_client.bind(on_press=self.controller.save_user)
        self.pop_client.open()

    def show_date_picker(self, *args):
        """this function allows you to choice a date in calendar"""
        date_dialog = MDDatePicker(min_date=datetime.date.today(),  max_date=datetime.date(2500, 1, 1))
        date_dialog.bind(on_save=self.save_date)
        date_dialog.open()

    def save_date(self, value, date, *args):
        """this function gets the value of date"""
        self.date_zone.text = str(date)

    def history_pop(self, *args):
        """this funnction displays details about addition, borrow or buy"""
        self.box_historique = BoxLayout(orientation="vertical", size_hint=(1, 1), padding=20, spacing=15)
        self.bar_btn = GridLayout(cols=3, rows=1, size_hint=(1, .2), spacing=10)
        self.btn_emprunt = BasicButton(text="Historique d'emprunt", font_size=18, background_color="blue")
        self.btn_emprunt.bind(on_press=self.load_borrowed_books_history)

        self.bar_btn.add_widget(self.btn_emprunt)
        self.btn_vendre = BasicButton(text="Historique de vendre", font_size=18, background_color="red")
        self.btn_vendre.bind(on_press=self.load_sold_books_history)

        self.bar_btn.add_widget(self.btn_vendre)
        self.btn_ajout = BasicButton(text="Historique d'ajout", font_size=18, background_color="green")
        self.btn_ajout.bind(on_press=self.load_added_books_history)
        self.bar_btn.add_widget(self.btn_ajout)

        self.box_historique.add_widget(self.bar_btn)

        self.liste_historique = GridLayout(size_hint_y=None, cols=1, row_default_height=50, padding=20, spacing=20)
        self.liste_historique.bind(minimum_height=self.liste_historique.setter("height"))

        self.scroll_historique = ScrollView()
        self.scroll_historique.add_widget(self.liste_historique)
        self.box_historique.add_widget(self.scroll_historique)
        # add l'history de l'ajout ds livres
        ##############################################
        # self.box_btn=BoxLayout(size_hint=(1,.15))
        # self.effacer=BasicButton(text="Effacer tous",font_size=18,size_hint=(.15,1),pos_hint={"center_x":.5})
        # self.box_btn.add_widget(self.effacer)
        # self.box_historique.add_widget(self.box_btn)
        self.pop_hist = Popup(title="Historique", content=self.box_historique, title_align="center", size_hint=(.7, .7))
        self.pop_hist.open()

    def load_added_books_history(self, *args):
        """this callback function returns to the controller concerning added books"""
        self.liste_historique.clear_widgets()
        self.controller.charger_add_book_historique()

    def load_sold_books_history(self, *args):
        """this callback function returns to the controller concerning deleted books"""
        self.liste_historique.clear_widgets()
        self.controller.charger_delete_book_historique()

    def load_borrowed_books_history(self, *args):
        """this callback function returns to the controller concerning borrowed books"""
        self.liste_historique.clear_widgets()
        self.controller.charger_emprunt_book_historique()
