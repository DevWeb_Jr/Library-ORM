from kivy.uix.gridlayout import GridLayout


class SalesResultsView(GridLayout):
    """this class concerns display graphic concerning sales results"""
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, **kwargs)
        self.cols = 1
        self.rows = 1
        self.controller = controller
        self.name = "Librairie | Bilan des ventes"
