from kivy.uix.anchorlayout import AnchorLayout
from UserInterface.StandardButton import ClassicButton

class FooterView(AnchorLayout):
    """this class concerns display of footer on every screen"""
    def __init__(self, controller, displayed=True, **kwargs):
        AnchorLayout.__init__(self, **kwargs)
        self.controller = controller
        self.anchor_x = "left"
        self.displayed = displayed
        self.display_return_button()


    def display_return_button(self):
        """this function adds a button on the grid provided for this purpose"""
        if self.displayed:
            return_button = ClassicButton(text="Retour")
            return_button.size_hint = (0.15, .75)
            return_button.bind(on_press=self.redirect_to_previous_screen)
            self.add_widget(return_button)

    def redirect_to_previous_screen(self, widget):
        """this function allows you to return to the previous screen"""
        self.controller.redirect_to_previous_screen_view()
