from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput


class CreateUpdateEmployeeView(BoxLayout):
    """this class concerns display about addition or modification of an employee"""
    def __init__(self, controller, **kwargs):
        super(CreateUpdateEmployeeView, self).__init__(**kwargs)
        self.controller = controller
        self.name = "Librairie | Créer ou modifier un compte"
        self.orientation = "vertical"
        self.pos_hint = {"center_x": .5, "center_y": .5}
        self.size_hint = (1, .8)

        self.grid_manager_libraire = GridLayout(cols=2, rows=1, size_hint=(.6, .1), spacing=30, padding=30, pos_hint={"center_x": .5})
        self.manager_button = Button(text="Manager", font_size=20)
        self.libraire_button = Button(text="Libraire", font_size=20)
        self.grid_manager_libraire.add_widget(self.manager_button)
        self.grid_manager_libraire.add_widget(self.libraire_button)

        self.add_widget(self.grid_manager_libraire)

        self.grid_username_password = GridLayout(cols=1, rows=2, size_hint=(.6, .15), padding=30, spacing=10, pos_hint={"center_x": .5})
        self.employee_name_input = TextInput(hint_text="name", font_size=20)
        self.employee_password_input = TextInput(hint_text="password", font_size=20)
        self.grid_username_password.add_widget(self.employee_name_input)
        self.grid_username_password.add_widget(self.employee_password_input)

        self.add_widget(self.grid_username_password)

        self.grid_update_cancel = GridLayout(cols=2, rows=1, size_hint=(.6, .1), padding=30, spacing=30, pos_hint={"center_x": .5})

        self.create_update_an_employee_button = Button(text="Valider", font_size=20)
        self.cancel_button = Button(text="Annuler", font_size=20)
        self.grid_update_cancel.add_widget(self.create_update_an_employee_button)
        self.grid_update_cancel.add_widget(self.cancel_button)

        self.add_widget(self.grid_update_cancel)
