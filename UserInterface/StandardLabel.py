from kivy.uix.label import Label


class StandardLabel(Label):
    def __init__(self, **kwargs):
        Label.__init__(self, **kwargs)
        pass


class TitleLabel(Label):
    def __init__(self, **kwargs):
        Label.__init__(self, **kwargs)
        self.color = "#3D6998"
        self.font_size = 40
        self.pos_hint = {"center_x": .5}
        self.bold = True
