from kivy.uix.gridlayout import GridLayout
from kivymd.app import MDApp
from Controller.FooterController import FooterController
from Controller.HeaderController import HeaderController
from Controller.LoginController import LoginController
from Controller.MainMenuController import MainMenuController
from Controller.BookDetailsController import BookDetailsController
from Controller.BookSearchController import BookSearchController
from Controller.BorrowBuyBookController import BorrowBuyBookController
from Controller.WarningBookController import WarningBookController
from Controller.OrderBookController import OrderBookController
from Controller.EmployeeManagementController import EmployeeManagementController
from Controller.CreateUpdateEmployeeController import CreateUpdateEmployeeController
from Controller.DeleteEmployeeController import DeleteEmployeeController
from Controller.SalesResultsController import SalesResultsController


class AppWindow(GridLayout):
    """This class concerns the display of all views by adding an header, a body and a footer"""
    def __init__(self, screen, **kwargs):
        GridLayout.__init__(self, **kwargs)
        self.screen = screen
        self.cols = 1
        self.rows = 3
        self.padding = 15

        self.header_grid = GridLayout(cols=1, rows=1, size_hint=(1, .15))
        self.add_widget(self.header_grid)

        self.body_grid = GridLayout(cols=1, rows=1, size_hint=(1, .6))
        self.add_widget(self.body_grid)

        self.footer_grid = GridLayout(cols=1, rows=1, size_hint=(1, .15))
        self.add_widget(self.footer_grid)

    def display_header(self, screen):
        """this function adds a header on the grid provided for this purpose"""
        self.header_grid.clear_widgets()
        self.header_grid.add_widget(screen)

    def display_body(self, screen):
        """this function adds a body on the grid provided for this purpose """
        self.body_grid.clear_widgets()
        self.body_grid.add_widget(screen)

    def display_footer(self, screen):
        """this function adds a footer on the grid provided for this purpose"""
        self.footer_grid.clear_widgets()
        self.footer_grid.add_widget(screen)


class AppScreen:
    """This class concerns the managing of application's views"""
    def __init__(self):

        self.app_window = AppWindow(self)

        self.header = HeaderController(self)

        self.footer = FooterController(self)

        self.log_in = LoginController(self)

        self.main_menu = MainMenuController(self)

        self.book_details = BookDetailsController(self)

        self.book_search = BookSearchController(self)

        self.borrow_buy_book = BorrowBuyBookController(self)

        self.warning_book = WarningBookController(self)

        self.order_book = OrderBookController(self)

        self.employee_management = EmployeeManagementController(self)

        self.create_update_employee = CreateUpdateEmployeeController(self)

        self.delete_employee = DeleteEmployeeController(self)

        self.sales_results = SalesResultsController(self)

        """this is where you enter the view you want to display first"""
        self.show_header(self.header.view.display_screen_title(text=self.log_in.view.name))
        self.show_body(self.log_in.view)
        #self.show_footer(self.footer.view)

    def show_header(self, screen):
        """This functions is a shortcut calling AppWindow header displaying function """
        self.app_window.display_header(screen)

    def show_body(self, screen):
        """This functions is a shortcut calling AppWindow body displaying function """
        self.app_window.display_body(screen)

    def show_footer(self, screen):
        """This functions is a shortcut calling AppWindow footer displaying function """
        self.app_window.display_footer(screen)


class Library(MDApp):
    """This class implements AppWindow and AppScreen and allows to start Application"""
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def build(self):
        self.theme_cls.theme_style = "Dark"
        self.title = "Bibliotheque"
        app_screen = AppScreen()
        return app_screen.app_window


if __name__ == '__main__':
    Library().run()
